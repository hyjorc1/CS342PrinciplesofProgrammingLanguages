package funclang;

import funclang.Value.RefVal;

public interface Heap {
	
	public Value ref(Value val);
	
	public Value deref(Value.RefVal loc);
	
	public Value setref(Value.RefVal loc, Value val);
	
	public Value free(Value.RefVal loc);
	
	static public class Heap16Bit implements Heap {

		private Value[] _rep = new Value[65536];
		int index;
		
		public Heap16Bit() {
			
		}
		
		@Override
		public Value ref(Value val) {
			if (index >= _rep.length)
				return new Value.DynamicError("err: out of memory");
			_rep[index] = val; 
			return new Value.RefVal(index++);
		}

		@Override
		public Value deref(RefVal loc) {
			try {
				return _rep[loc.loc()];
			} catch (Exception e) {
				return new Value.DynamicError("cant find that location");
			}
		}

		@Override
		public Value setref(RefVal loc, Value val) {
			try {
				_rep[loc.loc()] = val;
				return val;
			} catch (Exception e) {
				return new Value.DynamicError("cant find that location");
			}
		}

		@Override
		public Value free(RefVal loc) {
			try {
				_rep[loc.loc()] = null;
				return loc;
			} catch (Exception e) {
				return new Value.DynamicError("cant find that location");
			}
		}
		
	}
}
