grammar FuncLang;
 
import ListLang; //Import all rules from ListLang grammar.
 
 // New elements in the Grammar of this Programming Language
 //  - grammar rules start with lowercase

 exp returns [Exp ast]: 
		va=varexp { $ast = $va.ast; }
		| num=numexp { $ast = $num.ast; }
		| str=strexp { $ast = $str.ast; }
		| bl=boolexp { $ast = $bl.ast; }
        | add=addexp { $ast = $add.ast; }
        | sub=subexp { $ast = $sub.ast; }
        | mul=multexp { $ast = $mul.ast; }
        | div=divexp { $ast = $div.ast; }
        | let=letexp { $ast = $let.ast; }
        | lam=lambdaexp { $ast = $lam.ast; }
        | call=callexp { $ast = $call.ast; }
        | i=ifexp { $ast = $i.ast; }
        | less=lessexp { $ast = $less.ast; }
        | eq=equalexp { $ast = $eq.ast; }
        | gt=greaterexp { $ast = $gt.ast; }
        | car=carexp { $ast = $car.ast; }
        | cdr=cdrexp { $ast = $cdr.ast; }
        | cons=consexp { $ast = $cons.ast; }
        | list=listexp { $ast = $list.ast; }
        | nl=nullexp { $ast = $nl.ast; }
        
        | isnum=isnumexp { $ast = $isnum.ast; }
        | isbool=isboolexp { $ast = $isbool.ast; }
        | isstring=isstringexp { $ast = $isstring.ast; }
        | ispro=isprocedureexp { $ast = $ispro.ast; }
        | islist=islistexp { $ast = $islist.ast; }
        | ispair=ispairexp { $ast = $ispair.ast; }
        | isunit=isunitexp { $ast = $isunit.ast; }
        ;

 lambdaexp returns [LambdaExp ast] 
        locals [ArrayList<String> formals, java.util.Hashtable<String, Exp> map]
 		@init { $formals = new ArrayList<String>(); $map = new java.util.Hashtable<String, Exp>();} :
 		'(' Lambda 
 			'(' (id=Identifier { $formals.add($id.text); } | 
 			   '(' id=Identifier '=' e=exp ')' { $formals.add($id.text); $map.put($id.text, $e.ast);}
 			    )* 
 			')'
 			body=exp 
 		')' { $ast = new LambdaExp($formals, $body.ast, $map); }
 		;

 callexp returns [CallExp ast] 
        locals [ArrayList<Exp> arguments = new ArrayList<Exp>();  ] :
 		'(' f=exp 
 			( e=exp { $arguments.add($e.ast); } )* 
 		')' { $ast = new CallExp($f.ast,$arguments); }
 		;

 ifexp returns [IfExp ast] :
 		'(' If 
 		    e1=exp 
 			e2=exp 
 			e3=exp 
 		')' { $ast = new IfExp($e1.ast,$e2.ast,$e3.ast); }
 		;

 lessexp returns [LessExp ast] :
 		'(' Less 
 		    e1=exp 
 			e2=exp 
 		')' { $ast = new LessExp($e1.ast,$e2.ast); }
 		;

 equalexp returns [EqualExp ast] :
 		'(' Equal 
 		    e1=exp 
 			e2=exp 
 		')' { $ast = new EqualExp($e1.ast,$e2.ast); }
 		;

 greaterexp returns [GreaterExp ast] :
 		'(' Greater 
 		    e1=exp 
 			e2=exp 
 		')' { $ast = new GreaterExp($e1.ast,$e2.ast); }
 		;


 isnumexp returns [isNumExp ast] :
 		'(' Num 
 		    e=exp 
 		')' { $ast = new isNumExp($e.ast); }
 		;
 		
 isboolexp returns [isBoolExp ast] :
 		'(' 'boolean?' 
 		    e=exp 
 		')' { $ast = new isBoolExp($e.ast); }
 		;
 		
 isstringexp returns [isStringExp ast] :
 		'(' 'string?' 
 		    e=exp 
 		')' { $ast = new isStringExp($e.ast); }
 		;
 		
 isprocedureexp returns [isProcedureExp ast] :
 		'(' 'procedure?' 
 		    e=exp 
 		')' { $ast = new isProcedureExp($e.ast); }
 		;
 		
 islistexp returns [isListExp ast] :
 		'(' 'list?' 
 		    e=exp 
 		')' { $ast = new isListExp($e.ast); }
 		;
 		
 ispairexp returns [isPairExp ast] :
 		'(' 'pair?' 
 		    e=exp 
 		')' { $ast = new isPairExp($e.ast); }
 		;
 		
 isunitexp returns [isUnitExp ast] :
 		'(' 'unit?' 
 		    e=exp 
 		')' { $ast = new isUnitExp($e.ast); }
 		;
 		
 Num : 'number?' ;