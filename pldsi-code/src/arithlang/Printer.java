package arithlang;

import static arithlang.AST.*;

import arithlang.AST.MaddExp;
import arithlang.AST.MclrExp;
import arithlang.AST.MrecExp;
import arithlang.AST.PrimExp;
import arithlang.AST.Program;

public class Printer {
	public void print(Value v) {
		System.out.println(v.toString());
	}
	
	public static class Formatter implements AST.Visitor<String> {
		
		String expressionOf(Program p) {
			return (String) p.accept(this);
		}
		
		public String visit(Program p) {
			return (String) p.e().accept(this);
		}
		
		public String visit(NumExp e) {
			return "" + e.v();
		}
		
		public String visit(AddExp e) {
			String result = "(+";
			for(AST.Exp exp : e.all()) 
				result += " " + exp.accept(this);
			return result + ")";
		}		
		
		public String visit(SubExp e) {
			String result = "(-";
			for(AST.Exp exp : e.all()) 
				result += " " + exp.accept(this);
			return result + ")";
		}
		
		public String visit(MultExp e) {
			String result = "(*";
			for(AST.Exp exp : e.all()) 
				result += " " + exp.accept(this);
			return result + ")";
		}

		public String visit(DivExp e) {
			String result = "(/";
			for(AST.Exp exp : e.all()) 
				result += " " + exp.accept(this);
			return result + ")";
		}

		@Override
		public String visit(PrimExp e) {
			String result = "(prime ";
			result += e.e().accept(this); 
			return result + ")";
		}

		@Override
		public String visit(MrecExp e) {
			return "(Mrec)";
		}

		@Override
		public String visit(MclrExp e) {
			return "(Mclr)";
		}

		@Override
		public String visit(MaddExp e) {
			String result = "(M+";
			for(AST.Exp exp : e.all()) 
				result += " " + exp.accept(this);
			return result + ")";
		}
		
	}
}
