package arithlang;

import java.util.List;
import arithlang.AST.AddExp;
import arithlang.AST.DivExp;
import arithlang.AST.Exp;
import arithlang.AST.MaddExp;
import arithlang.AST.MclrExp;
import arithlang.AST.MrecExp;
import arithlang.AST.MultExp;
import arithlang.AST.NumExp;
import arithlang.AST.PrimExp;
import arithlang.AST.Program;
import arithlang.AST.SubExp;
import arithlang.AST.Visitor;
import arithlang.Value.NumVal;

public class ASTCounter implements Visitor<Value> {
	
	int numberof(Program p) {
		return (int)((NumVal)p.accept(this)).v() + 1;
	}
	
	@Override
	public Value visit(AddExp e) {
		List<Exp> operands = e.all();
		double count = 1;
		for(int i=0; i<operands.size(); i++) {
			NumVal val = (NumVal) operands.get(i).accept(this);
			count += val.v();
		}
		return new NumVal(count);
	}

	@Override
	public Value visit(NumExp e) {
		return new NumVal(1);
	}

	@Override
	public Value visit(DivExp e) {
		List<Exp> operands = e.all();
		double count = 1;
		for(int i=0; i<operands.size(); i++) {
			NumVal val = (NumVal) operands.get(i).accept(this);
			count += val.v();
		}
		return new NumVal(count);
	}

	@Override
	public Value visit(MultExp e) {
		List<Exp> operands = e.all();
		double count = 1;
		for(int i=0; i<operands.size(); i++) {
			NumVal val = (NumVal) operands.get(i).accept(this);
			count += val.v();
		}
		return new NumVal(count);
	}

	@Override
	public Value visit(Program p) {
		return (NumVal) p.e().accept(this);
	}

	@Override
	public Value visit(SubExp e) {
		List<Exp> operands = e.all();
		double count = 1;
		for(int i=0; i<operands.size(); i++) {
			NumVal val = (NumVal) operands.get(i).accept(this);
			count += val.v();
		}
		return new NumVal(count);
	}


	@Override
	public Value visit(PrimExp e) {
		return new NumVal(2);
	}

	@Override
	public Value visit(MrecExp e) {
		return new NumVal(2);
	}

	@Override
	public Value visit(MclrExp e) {
		return new NumVal(2);
	}

	@Override
	public Value visit(MaddExp e) {
		return new NumVal(2);
	}


}
