package arithlang;
import static arithlang.AST.*;
import static arithlang.Value.*;

import java.util.List;

import arithlang.AST.MaddExp;
import arithlang.AST.MclrExp;
import arithlang.AST.MrecExp;
import arithlang.AST.PrimExp;

public class Evaluator implements Visitor<Value> {
	
	Printer.Formatter ts = new Printer.Formatter();
	
	private NumVal pre = new NumVal(0);
	
	Value valueOf(Program p) {
		// Value of a program in this language is the value of the expression
		return (Value) p.accept(this);
	}
	
	@Override
	public Value visit(AddExp e) {
		List<Exp> operands = e.all();
		double result = 0;
		for(Exp exp: operands) {
			NumVal intermediate = (NumVal) exp.accept(this); // Dynamic type-checking
			result += intermediate.v(); //Semantics of AddExp in terms of the target language.
		}
		return new NumVal(result);
	}

	@Override
	public Value visit(NumExp e) {
		return new NumVal(e.v());
	}

	@Override
	public Value visit(DivExp e) {
		List<Exp> operands = e.all();
		NumVal lVal = (NumVal) operands.get(0).accept(this);
		double result = lVal.v(); 
		for(int i=1; i<operands.size(); i++) {
			NumVal rVal = (NumVal) operands.get(i).accept(this);
			result = result / rVal.v();
		}
		return new NumVal(result);
	}

	@Override
	public Value visit(MultExp e) {
		List<Exp> operands = e.all();
		double result = 1;
		for(Exp exp: operands) {
			NumVal intermediate = (NumVal) exp.accept(this); // Dynamic type-checking
			result *= intermediate.v(); //Semantics of MultExp.
		}
		return new NumVal(result);
	}

	@Override
	public Value visit(Program p) {
		return (Value) p.e().accept(this);
	}

	@Override
	public Value visit(SubExp e) {
		List<Exp> operands = e.all();
		NumVal lVal = (NumVal) operands.get(0).accept(this);
		double result = lVal.v();
		for(int i=1; i<operands.size(); i++) {
			NumVal rVal = (NumVal) operands.get(i).accept(this);
			result = result - rVal.v();
		}
		return new NumVal(result);
	}

	@Override
	public Value visit(PrimExp e) {
		NumVal val = (NumVal) e._e.accept(this);
		if (val.v() < 0) 
			return null;
		return new NumVal(0);
	}

	@Override
	public Value visit(MrecExp e) {
		return pre;
	}

	@Override
	public Value visit(MclrExp e) {
		pre = new NumVal(0);
		return pre;
	}

	@Override
	public Value visit(MaddExp e) {
		double totalVal = pre.v();
		List<Exp> operands = e.all();
		for(int i=1; i<operands.size(); i++) {
			totalVal += ((NumVal) operands.get(i).accept(this)).v();
		}
		return new NumVal(totalVal);
	}

}
