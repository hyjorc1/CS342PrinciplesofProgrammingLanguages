package typelang;

import java.util.ArrayList;
import java.util.List;

import typelang.AST.*;
import typelang.Env.ExtendEnv;
import typelang.Env.GlobalEnv;
import typelang.Type.*;

public class Checker implements Visitor<Type,Env<Type>> {
	Printer.Formatter ts = new Printer.Formatter();
	Env<Type> initEnv = initialEnv(); //New for definelang
	
	private Env<Type> initialEnv() {
		GlobalEnv<Type> initEnv = new GlobalEnv<Type>();
		
		/* Type for procedure: (read <filename>). Following is same as (define read (lambda (file) (read file))) */
		List<Type> formalTypes = new ArrayList<Type>();
		formalTypes.add(new Type.StringT());
		initEnv.extend("read", new Type.FuncT(formalTypes, new Type.StringT()));

		/* Type for procedure: (require <filename>). Following is same as (define require (lambda (file) (eval (read file)))) */
		formalTypes = new ArrayList<Type>();
		formalTypes.add(new Type.StringT());
		initEnv.extend("eval", new Type.FuncT(formalTypes, new Type.UnitT()));
		
		/* Add type for new built-in procedures here */ 
		
		return initEnv;
	}
	
    Type check(Program p) {
		return (Type) p.accept(this, null);
	}

	public Type visit(Program p, Env<Type> env) {
		Env<Type> new_env = initEnv;

		for (DefineDecl d: p.decls()) {
			Type type = (Type)d.accept(this, new_env);

			if (type instanceof ErrorT) { return type; }

			Type dType = d.type();

			if (!type.typeEqual(dType)) {
				return new ErrorT("Expected " + dType + " found " + type + " in " + ts.visit(d, null));
			}

			new_env = new ExtendEnv<Type>(new_env, d.name(), dType);
		}
		return (Type) p.e().accept(this, new_env);
	}

	public Type visit(VarExp e, Env<Type> env) {
		try {
			return env.get(e.name());
		} catch(Exception ex) {
			return new ErrorT("Variable " + e.name() +
					" has not been declared in " + ts.visit(e, null));
		}
	}

	public Type visit(LetExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("LetExp has not been implemented");
	}

	public Type visit(DefineDecl d, Env<Type> env) {
		String name = d.name();
		Type t =(Type) d._value_exp.accept(this, env);
		((GlobalEnv<Type>) initEnv).extend(name, t);
		return t;
	}

	public Type visit(LambdaExp e, Env<Type> env) {
		List<String> names = e.formals();
		List<Type> types = e.types();
		String message = "The number of formal parameters and the number of "
				+ "arguments in the function type do not match in ";
		if (types.size() == names.size()) {
			Env<Type> new_env = env;
			int index = 0;
			for (Type argType : types) { 
				new_env = new ExtendEnv<Type>(new_env, names.get(index),
						argType);
				index++;
			}
			Type bodyType = (Type) e.body().accept(this, new_env);
			return new FuncT(types,bodyType);
		}
		return new ErrorT(message + ts.visit(e, null));
	}

	public Type visit(CallExp e, Env<Type> env) {
		Exp operator = e.operator();
		List<Exp> operands = e.operands();

		Type type = (Type)operator.accept(this, env);
		if (type instanceof ErrorT) { return type; }

		String message = "Expect a function type in the call expression, found "
				+ type.tostring() + " in ";
		if (type instanceof FuncT) {
			FuncT ft = (FuncT)type;

			List<Type> argTypes = ft.argTypes();
			int size_actuals = operands.size();
			int size_formals = argTypes.size();

			message = "The number of arguments expected is " + size_formals +
					" found " + size_actuals + " in ";
			if (size_actuals == size_formals) {
				for (int i = 0; i < size_actuals; i++) {
					Exp operand = operands.get(i);
					Type operand_type = (Type)operand.accept(this, env);

					if (operand_type instanceof ErrorT) { return operand_type; }

					if (!assignable(argTypes.get(i), operand_type)) {
						return new ErrorT("The expected type of the " + i +
								" argument is " + argTypes.get(i).tostring() +
								" found " + operand_type.tostring() + " in " +
								ts.visit(e, null));
					}
				}
				return ft.returnType();
			}
		}
		return new ErrorT(message + ts.visit(e, null));
	}

	public Type visit(LetrecExp e, Env<Type> env) {
		List<String> names = e.names();
		List<Type> types = e.types();
		List<Exp> fun_exps = e.fun_exps();

		// collect the environment
		Env<Type> new_env = env;
		for (int index = 0; index < names.size(); index++) {
			new_env = new ExtendEnv<Type>(new_env, names.get(index),
					types.get(index));
		}

		// verify the types of the variables
		for (int index = 0; index < names.size(); index++) {
			Type type = (Type)fun_exps.get(index).accept(this, new_env);

			if (type instanceof ErrorT) { return type; }

			if (!assignable(types.get(index), type)) {
				return new ErrorT("The expected type of the " + index +
						" variable is " + types.get(index).tostring() +
						" found " + type.tostring() + " in " +
						ts.visit(e, null));
			}
		}

		return (Type) e.body().accept(this, new_env);
	}

	public Type visit(IfExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("IfExp has not been implemented");
	}

	public Type visit(CarExp e, Env<Type> env) {
		Exp carExp = e.arg();
		Type expType = (Type)carExp.accept(this, env);
		if (expType instanceof ErrorT)
			return expType;
		if (expType instanceof PairT)
			return ((PairT)expType).fst();
		return new ErrorT("The car expect an expression of type Pair, found " 
			+ expType.tostring() + " in " + ts.visit(e, null));
	}

	public Type visit(CdrExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("CdrExp has not been implemented");
	}

	public Type visit(ConsExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("ConsExp has not been implemented");
	}

	public Type visit(ListExp e, Env<Type> env) {
		List<Exp> list = e.elems();
		Type listType = e.type();
		for (int i = 0; i < list.size(); i++) {
			Exp ele = list.get(i);
			Type eleType = (Type)ele.accept(this, env);
			if (eleType instanceof ErrorT) 
				return eleType;
			if (!assignable(listType, eleType)) 
				return new ErrorT("The " + i + " expression should have type " 
			+ listType.tostring() + " found " + eleType.tostring() 
			+ " in " + ts.visit(e, null));
		}
		return new ListT(listType);
	}

	public Type visit(NullExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("NullExp has not been implemented");
	}

	public Type visit(RefExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("RefExp has not been implemented");
	}

	public Type visit(DerefExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("DerefExp has not been implemented");
	}

	public Type visit(AssignExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("AssignExp has not been implemented");
	}

	public Type visit(FreeExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("FreeExp has not been implemented");
	}

	public Type visit(UnitExp e, Env<Type> env) {/*
		 * Your code goes here
		 */
		return new ErrorT("UnitExp has not been implemented");}

	public Type visit(NumExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("NumExp has not been implemented");
	}

	public Type visit(StrExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("StrExp has not been implemented");
	}

	public Type visit(BoolExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("BoolExp has not been implemented");
	}

	public Type visit(LessExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("LessExp has not been implemented");
	}

	public Type visit(EqualExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("EqualExp has not been implemented");
	}

	public Type visit(GreaterExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("GreaterExp has not been implemented");
	}

	private Type visitBinaryComparator(BinaryComparator e, Env<Type> env,
			String printNode) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("BinaryComparator has not been implemented");
	}


	public Type visit(AddExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("AddExp has not been implemented");
	}

	public Type visit(DivExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("DivExp has not been implemented");
	}

	public Type visit(MultExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("MultExp has not been implemented");
	}

	public Type visit(SubExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("SubExp has not been implemented");
	}

	private Type visitCompoundArithExp(CompoundArithExp e, Env<Type> env, String printNode) {
		/*
		 * Your code goes here
		 */
		return new ErrorT("CompoundArithExp has not been implemented");
	}

	private static boolean assignable(Type t1, Type t2) {
		if (t2 instanceof UnitT) { return true; }

		return t1.typeEqual(t2);
	}
	
	public Type visit(ReadExp e, Env<Type> env) {
		return UnitT.getInstance();
	}

	public Type visit(EvalExp e, Env<Type> env) {
		return UnitT.getInstance();
	}
}
